import pandas as pd
from glob import glob
import os


def merge_csv(ready_for_slice_dir:str, merged_csv_dir:str):
    csv_combined = pd.DataFrame()

    for entry in glob (os.path.join(ready_for_slice_dir, '/*.csv')):
        df = pd.read_csv(entry)
        csv_combined = pd.concat([csv_combined, df], ignore_index=True)

    csv_combined.to_csv(os.path.join(merged_csv_dir, '/Full_Transcript.csv'), header=True, index=False, encoding='utf-8-sig')
    print('All csv-files merged')
