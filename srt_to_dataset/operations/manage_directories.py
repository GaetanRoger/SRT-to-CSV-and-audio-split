from tempfile import mkdtemp
from os import mkdir, path
from shutil import rmtree


# Create working and output directories
def create_directories(output_csv_dir: str, output_sliced_audio_dir: str):
    ready_for_slice_dir = mkdtemp()
    merged_csv_dir = mkdtemp()
    output_csv_dir = mkdir(output_csv_dir, exist_ok=True)
    output_sliced_audio_dir = mkdir(output_sliced_audio_dir, exist_ok=True)

    return ready_for_slice_dir, merged_csv_dir, output_csv_dir, output_sliced_audio_dir


# Checking if SRT and audio directories exist (and exists of not)
def check_directories(srt_dir: str, audio_dir: str):
    if not path.isdir(srt_dir):
        print(f"SRT directory {srt_dir} does not exist.")
        exit()

    if not path.isdir(audio_dir):
        print(f"Audio directory {audio_dir} does not exist.")
        exit()


# Removing temporary directories
def delete_temp_directories(ready_for_slice_dir, merged_csv_dir):
    rmtree(ready_for_slice_dir)
    rmtree(merged_csv_dir)