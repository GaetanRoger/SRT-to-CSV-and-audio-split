import librosa
import soundfile
import os
import time
import sys
import shutil

def pre_process_audio(audio_path, ready_for_slice_dir):
    start_sub = time.time()
    n = 0
    print('Downsampling wav files...')
    for file in os.listdir(audio_path):
        if(file.endswith('.wav')):
            try:
                nameSolo_1 = file.rsplit('.', 1)[0]
                y, s = librosa.load(audio_path + file, sr=16000) # Downsample 44.1kHz to 8kHz
                soundfile.write(os.path.join(ready_for_slice_dir, nameSolo_1 + '.wav'), y, s)
                n = n+1
                print('File ', n , ' completed:', nameSolo_1)
            except EOFError as error:
                next

    print('Downsampling complete')
    print('---------------------------------------------------------------------')

    s = 0
    print('Changing bit pro sample...')
    for file in os.listdir(ready_for_slice_dir):
        if(file.endswith('.wav')):
            try:
                nameSolo_2 = file.rsplit('.', 1)[0]
                #nameSolo_2 = nameSolo_2.replace('')
                data, samplerate = soundfile.read(os.path.join(ready_for_slice_dir, file))
                soundfile.write(os.path.join(ready_for_slice_dir, nameSolo_2 + '.wav'), data, samplerate, subtype='PCM_16')
                s = s + 1
                print('File ' , s , ' completed')
            except EOFError as error:
                next

    print('Bit pro sample changed')
    print('---------------------------------------------------------------------')

    #shutil.rmtree('./audio', ignore_errors=True)

    end_sub = time.time()

    print('The script took ', end_sub-start_sub, ' seconds to run')

#Source:
#https://stackoverflow.com/questions/30619740/python-downsampling-wav-audio-file
#https://stackoverflow.com/questions/44812553/how-to-convert-a-24-bit-wav-file-to-16-or-32-bit-files-in-python3
