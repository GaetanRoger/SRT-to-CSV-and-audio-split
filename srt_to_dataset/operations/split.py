import numpy as np
import pandas as pd
import os


def split_dataset(to_split_path, merged_csv_dir:str, output_dir:str):

    df = pd.read_csv(os.path.join(merged_csv_dir, to_split_path))

    def train_validate_test_split_CV(df, train_percent=.75, validate_percent=.1, seed=None):
        np.random.seed(seed)
        perm = np.random.permutation(df.index)
        m = len(df.index)
        train_end = int(train_percent * m)
        validate_end = int(validate_percent * m) + train_end
        train = df.iloc[perm[:train_end]]
        validate = df.iloc[perm[train_end:validate_end]]
        test = df.iloc[perm[validate_end:]]
        return train, validate, test

    train, validate, test = train_validate_test_split_CV(df)

    train.to_csv(os.path.join(output_dir, 'train.csv'), header=True, index=False, encoding='utf-8-sig')
    validate.to_csv(os.path.join(output_dir, 'dev.csv'), header=True, index=False, encoding='utf-8-sig')
    test.to_csv(os.path.join(output_dir, 'test.csv'), header=True, index=False, encoding='utf-8-sig')
