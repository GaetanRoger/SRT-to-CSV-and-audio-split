#Create csv with filepath and -size in preparation for final DS training-csv

import pandas as pd
import os
from glob import glob
import wave
import contextlib


def create_DS_csv (sliced_audio_dir):
    #this function holds the code to extract the filepath and filesize of all audio in the respective directory
    data = pd.DataFrame(columns=['wav_filename', 'wav_filesize', 'duration'])
    df = pd.DataFrame(columns=['wav_filename', 'wav_filesize', 'duration'])

    for entry in glob(os.path.join(sliced_audio_dir, '/*.wav')):
        filepath = os.path.abspath(entry)
        filesize = os.path.getsize(entry)
        with contextlib.closing(wave.open(entry, 'rb')) as f:
            frames = f.getnframes()
            rate = f.getframerate()
            duration = frames / float(rate)
        df['wav_filename'] = [filepath]
        df['wav_filesize'] = [filesize]
        df['duration'] = [duration]
        data = pd.concat([data, df], ignore_index=True)
    data.to_csv('./merged_csv/Filepath_Filesize.csv', header=True, index=False, encoding='utf-8-sig')