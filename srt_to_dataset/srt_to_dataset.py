from glob import glob
from time import time
from os import path
import pandas as pd
import datetime

from .operations.manage_directories import check_directories, create_directories, delete_temp_directories
from .operations.convert_srt_to_csv import convert_srt_to_csv
from .operations.change_sample_rate import pre_process_audio
from .operations.slice_audio import split_files
from .operations.create_DS_csv import create_DS_csv
from .operations.merge_csv import merge_csv
from .operations.merge_transcripts_and_files import merge_transcripts_and_wav_files
from .operations.clean import clean_unwanted_characters
from .operations.trans_numbers import translate_numbers
from .operations.split import split_dataset
from .operations.audio_metrics import audio_metrics


def run_srt_to_dataset(
    input_srt_dir: str,
    input_audio_dir: str,
    output_csv_dir: str,
    output_sliced_audio_dir: str,
):
    # <==> Step 0 <==>
    #
    # Configuration and setup


    # Starting timer to measure execution time
    start_time = time()

    # Checking if SRT and audio directories exist, and
    # creating needed working and output directories
    check_directories(input_srt_dir, input_audio_dir)
    ready_for_slice_dir, merged_csv_dir, output_csv_dir, output_sliced_audio_dir = create_directories(output_csv_dir, output_sliced_audio_dir)

    # Creating working variables
    input_srt_files_paths = glob(path.join(input_srt_dir, '*.srt'))

    # Counting SRT files to convert
    srt_counter = len(input_srt_files_paths)


    # <==> Step 1 <==>
    #
    # Extracting information from srt-files to csv
    print('1. Extracting information from srt_file(s) to csv_files')
    for srt_file in input_srt_files_paths:
        convert_srt_to_csv(srt_file, ready_for_slice_dir)
    print('%s-file(s) converted and saved as csv-files to ./csv' %srt_counter)
    print('---------------------------------------------------------------------')


    # <==> Step 2 <==>
    #
    # Pre-process audio for folder in which wav files are stored
    pre_process_audio(input_audio_dir, ready_for_slice_dir)
    print('2. Pre-processing of audio files is complete.')
    print('---------------------------------------------------------------------')


    # <==> Step 3 <==>
    #
    # Now slice audio according to start- and end-times in csv
    print('3. Slicing audio according to start- and end_times of transcript_csvs...')
    for csv_file in glob(path.join(ready_for_slice_dir, '/*.csv')):
        wav_file = csv_file.replace('.csv','.wav')
        if path.exists(wav_file):
            split_files(csv_file, wav_file, output_sliced_audio_dir)
        else:
            next
    wav_counter = len(glob(path.join(output_sliced_audio_dir, '/*.wav')))
    print('Slicing complete. {} files in dir "sliced_audio"'.format(wav_counter))
    print('---------------------------------------------------------------------')


    # <==> Step 4 <==>
    #
    # Now create list of filepaths and -size of dir ./sliced_audio
    print('4. Extracting filepath and -size for every .wav file in sliced audio dir')
    create_DS_csv(output_sliced_audio_dir)
    print('DS_csv with Filepaths - and sizes created.')
    print('---------------------------------------------------------------------')


    # <==> Step 5 <==>
    #
    # Now join all seperate csv files
    print('5. Merging csv-files with transcriptions')
    merge_csv(ready_for_slice_dir, merged_csv_dir)
    print('Merged csv with all transcriptions created.')
    print('---------------------------------------------------------------------')


    # <==> Step 6 <==>
    #
    # Merge the csv with transcriptions and the file-csv with paths and sizes
    transcript_path = path.join(merged_csv_dir, '/Full_Transcript.csv')
    DS_path = path.join(merged_csv_dir, '/Filepath_Filesize.csv')
    print('6. Merging transcripts and WAV files')
    merge_transcripts_and_wav_files(transcript_path, DS_path, merged_csv_dir)
    print('Final DS csv generated.')
    print('---------------------------------------------------------------------')

    # <==> Step 7 <==>
    #
    # Clean the data of unwanted characters and translate numbers from int to words
    print('7. Cleaning texts')
    final_csv_path = 'DS_training_final.csv'
    cleaned_csv_path = 'DS_training_final_char_removed.csv'
    clean_unwanted_characters(final_csv_path, merged_csv_dir)
    translate_numbers(cleaned_csv_path, merged_csv_dir)
    print('Unwanted characters cleaned and numbers translated to words.')
    print('---------------------------------------------------------------------')


    # <==> Step 8 <==>
    #
    # Write transcript to text-file for language model
    print(f'8. Generating datasets into directory {output_csv_dir}')
    df_text = pd.read_csv(path.join(merged_csv_dir, '/DS_training_final_char_removed_cleaned.csv'))
    df_text['transcript'].to_csv(path.join(output_csv_dir, '/txt_for_lm.txt'), header=None, index=None, mode='a')

    # Now split the data into three subsets
    to_split_path = 'DS_training_final_char_removed_cleaned.csv'
    split_dataset(to_split_path, merged_csv_dir, output_csv_dir)
    print('Datasplit completed.')


    print('********************************************** FINISHED ************************************************')
    print('The preprocessing of wmv or mp4 files and their corresponding srt files is completed')
    print('Final processed audio is in ./sliced_audio and final csv-files in ./final_csv')
    print('******************************************* AUDIO METRICS **********************************************')
    if path.isdir(merged_csv_dir) is True and path.isdir(output_csv_dir) is True:
        audio_metrics(merged_csv_dir, output_csv_dir)
    print('******************************************* Execution Time *********************************************')
    #evaluate the scripts execution time
    end_time = time.time()
    exec_time = str(datetime.timedelta(seconds=end_time-start_time))

    print('The script took {} to run'.format(exec_time))
    print('********************************************************************************************************')



    delete_temp_directories(ready_for_slice_dir, merged_csv_dir)
